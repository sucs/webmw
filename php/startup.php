<?
require_once("config.php");

$mwpoll = $poller_bin;
$username = trim($_REQUEST['username']);
$password = trim($_REQUEST['password']);

// Preliminary checks

// Test that the login details make sense.
if (empty($username) || empty($password)) {
  echo json_encode((object) ['error' => 'username and password must both be not empty']);
}

if (!preg_match("/^[a-z][a-z0-9_]*$/", $username)) {
  echo json_encode((object) ['error' => 'invalid characters in username']);
  exit;
}

login($mwpoll, $username, $password, true);

function login($mwpoll, $username, $password, $continue) {
  $desc = array( 
    0 => array("pipe", "r"),
    1 => array("pipe", "w"),
  );
  $pipes = array();

  $p = proc_open($mwpoll." -q -u $username", $desc, $pipes);

  if (empty($p)) {
    echo "<h1>No</h1>";	
    exit;
  }

  if ($p === FALSE) {
    echo json_encode((object) ['error' => 'failed to exec mwpoll']);
    exit;
  }

  // Try logging on using username and password
  if (fwrite($pipes[0], $password."\n") === FALSE) {
    echo "Error writing to mwpoll\n";
  }

  $pid = fgets($pipes[1]);
  if ($pid === FALSE) {
    echo "error reading pid.\n";
    exit;
  }

  $pid = trim($pid);
  if (!is_numeric($pid)) {
    if (preg_match('/not found/', $pid)) {
      if ($continue) {
	createUser($mwpoll, $username, $password);
      }
      exit;
    } else {
      echo json_encode((object) ['error' => 'Bad response: pid=$pid']);
      exit;
    }
  }
  
  $auth = fgets($pipes[1]);

  if ($auth === FALSE) {
    echo "Error reading auth string\n";
  }

  $sess = array (
    "pid" => $pid,
    "auth" => $auth,
    "username" => $username
  );

  $mwsess = serialize($sess);
  echo "success:" . $mwsess;
}

function createUser($mwpoll, $username, $password) {
  // SUCS LDAP Server
  $sucsLDAPServer = 'silver.sucs.swan.ac.uk';
  // Bind String
  $sucsBindDn = "uid=$username,ou=People,dc=sucs,dc=org";

  // Try and connect to Silver
  $ldapconnSUCS = ldap_connect($sucsLDAPServer) or die("Could not connect to SUCS LDAP server.");

  if ($ldapconnSUCS) {
    // Try and bind to SUCS LDAP. Using @ to suppress PHP warining on failure
    $ldapbindSUCS = @ldap_bind($ldapconnSUCS, $sucsBindDn, $password);

    if ($ldapbindSUCS) {
      $desc = array( 
	0 => array("pipe", "r"),
	1 => array("pipe", "w"),
      );
      $pipes = array();
      // Authed to SUCS lets try and create the user
      $p = proc_open($mwpoll." -q -a -u $username -s", $desc, $pipes);
      fwrite($pipes[0], $password . "\n");
      fwrite($pipes[0], $password . "\n");
      fclose($pipes[0]);
      $pid = fgets($pipes[1]);
      if ($pid === FALSE) {
	echo json_encode((object) ['error' => 'Error reading pid']);
      }

      $auth = fgets($pipes[1]);
      fclose($pipes[1]);
      proc_close($p);
      if ($auth === FALSE) {
	login($mwpoll, $username, $password, false);
      }
      exit;
    } else {
      echo json_encode((object) ['error' => 'Failed to auth to SUCS']);
      exit;
    }
  } else {
    echo json_encode((object) ['error' => 'Failed to connect to SUCS LDAP']);
    exit;
  }
}
?>
