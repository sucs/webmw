// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueLocalStorage from 'vue-ls'
import VueLocalForage from 'vue-localforage'

var options = {
  namespace: 'vuejs__'
}

Vue.use(VueLocalStorage, options)
Vue.use(VueLocalForage)

window.bus = new Vue()

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
